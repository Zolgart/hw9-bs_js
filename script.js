/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
4. Яка різниця між nodeList та HTMLCollection?

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

/*Теоретичні питання

1.DOM - це структура, яка представляє сторінку в браузері у вигляді дерева об'єктів. 
Кожен елемент сторінки є об'єктом в цьому дереві, і взаємодія з ним може бути здійснена за допомогою JavaScript.

2.innerHTML визначає або повертає HTML вміст внутрішнього елементу, в той час як innerText працює з текстовим вмістом і ігнорує HTML.

3.Елемент можна знайти за допомогою методів, таких як getElementById, getElementsByClassName, getElementsByTagName,
 або за допомогою сучасних методів, таких як querySelector та querySelectorAll.

4.Обидва представляють колекції елементів, але nodeList може містити будь-які типи вузлів (елементи, атрибути), 
тоді як HTMLCollection представляє тільки елементи.
*/


    // Практичні завдання

   
//    1
    const features2 = document.querySelectorAll('.feature'); 

    console.log(features1);
    console.log(features2);

    
    features1.forEach(feature => feature.style.textAlign = 'center');

    // 2. 
    const h2Elements = document.querySelectorAll('h2');
    h2Elements.forEach(h2 => h2.textContent = 'Awesome feature');

    // 3. 
    const titleElements = document.querySelectorAll('.feature-title');
    titleElements.forEach(title => title.textContent += '!');
 